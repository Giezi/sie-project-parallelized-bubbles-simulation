# improt custom CUDA class
import particleSimulation as ps
import numpy as np


"""
This is the function wrapper that calls the CUDA code
The reason for this wrapper is because of the way SWIG handles outputs. Since SWIG needs the length
of the output, this is computed here.
-----
Input:	x: 			initial position in x [mm]
		y: 			initial position in y [mm]
		z: 			initial position in z [mm]
		N: 			number of iterations to be done in the kernel before returning a result
		dt: 		number of seconds per time step [s]
		up: 		velocity in the x-direction [mm/s]
		vp: 		velocity in the y-direction [mm/s]
		wp: 		velocity in the z-direction [mm/s]

		fp_type:	Defines the fluid type (0 = gas, 1 = liquid) that is expected to be
			        contained in the bubble.  This is needed because the Peng-Robinson
			        equation of state returns values for both phases of a mixture.  This
			        variable allows the class to automatically return the values for the
			        desired phase. Default is set to zero.
		T : 		temperature (K)
		P: 			pressure (Pa)
		Sa: 		sea salinity
		Ta : 		sea temperature
		m:			array of masses for each component in the mixture (kg)
		Mol_wt: 	array of molecular weights for each component (kg/mol)
		Pc: 		array of critical point pressures for each component (Pa)
		Tc: 		array of critical point temperatures for each component (K)
		omega: 		array of Pitzer acentric factors for each component (--)
		delta: 		atrix of binary interaction coefficients (--), default is zero.

Output:	xt: x position after N iterations
		yt: y position after N iterations
		zt: z position after N iterations
"""
def getNewParticlePosition(x,y,z,N,dt,up,vp,wp,T,P,Sa,Ta,m,Mol_wt,Pc,Tc,omega,fp_type = 0, delta = None):
	if delta is None:
		delta = np.zeros((len(m),len(m)), dtype=np.float32);
	aDelta = delta.ravel();
	(xt,yt,zt) = ps.doCUDAcomp(x,y,z,m, Mol_wt, Pc, Tc, omega, aDelta, N,fp_type,dt,up,vp,wp, T, P, Sa, Ta, len(x),len(y),len(z))
	return xt,yt,zt
