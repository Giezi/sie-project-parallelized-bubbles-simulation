#include <kernel.cu>
#include <particleSimulation.h>
#include "stdio.h"
#include <curand_kernel.h>
#include <time.h>
#include <stdlib.h>


//this is the main function that launches the kernels
void doCUDAcomp(int Nparticles,float* x_in,float* y_in, float* z_in,int nc, float* m, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta,int Niter, int fp_type,float dt,float up,float vp,float wp, float T, float P, float Sa, float Ta, float* x_out, float* y_out, float* z_out)
{
	//define constants for the run
	//numThreadsPerBlock: number of threads that one block will handle in the GPU
	//numBlocks: number of blocks that the GPU will use. This is dependent on the number of particles and the number of threads.
	int numThreadsPerBlock = 256;
	int numBlocks = (Nparticles+numThreadsPerBlock-1)/numThreadsPerBlock;

	//set the heap size
	cudaDeviceSetLimit(cudaLimitMallocHeapSize,Nparticles*nc*100*sizeof(float));

	//allocate memory on the device (GPU) for the generation of random numbers
	curandState* devStates;
	cudaMalloc ( &devStates, Nparticles*sizeof( curandState ) );

	//Launch the kernel which initiates the random numbers.
	//This has to be done before allocating the memory for the particles position, otherwise some errors might occure.
	kernel_setupRandom <<< numBlocks, numThreadsPerBlock >>> ( devStates, rand() + static_cast<int>(time(NULL)));

	//Allocate and copy the positions of the particles from the host (CPU) to the device (GPU)
	float *d_x_in;
	cudaMalloc((void**)&d_x_in,Nparticles*sizeof(float));
	cudaMemcpy(d_x_in,x_in,Nparticles*sizeof(float),cudaMemcpyHostToDevice);

	float *d_x_out;
	cudaMalloc((void**)&d_x_out,Nparticles*sizeof(float));

	float *d_y_in;
	cudaMalloc((void**)&d_y_in,Nparticles*sizeof(float));
	cudaMemcpy(d_y_in,y_in,Nparticles*sizeof(float),cudaMemcpyHostToDevice);

	float *d_y_out;
	cudaMalloc((void**)&d_y_out,Nparticles*sizeof(float));

	float *d_z_in;
	cudaMalloc((void**)&d_z_in,Nparticles*sizeof(float));
	cudaMemcpy(d_z_in,z_in,Nparticles*sizeof(float),cudaMemcpyHostToDevice);

	float *d_z_out;
	cudaMalloc((void**)&d_z_out,Nparticles*sizeof(float));

	//Allocate and copy arrays from input to device
	float *d_m;
	cudaMalloc((void**)&d_m,nc*sizeof(float));
	cudaMemcpy(d_m,m,nc*sizeof(float),cudaMemcpyHostToDevice);

	float *d_Mol_wt;
	cudaMalloc((void**)&d_Mol_wt,nc*sizeof(float));
	cudaMemcpy(d_Mol_wt,Mol_wt,nc*sizeof(float),cudaMemcpyHostToDevice);

	float *d_Pc;
	cudaMalloc((void**)&d_Pc,nc*sizeof(float));
	cudaMemcpy(d_Pc,Pc,nc*sizeof(float),cudaMemcpyHostToDevice);

	float *d_Tc;
	cudaMalloc((void**)&d_Tc,nc*sizeof(float));
	cudaMemcpy(d_Tc,Tc,nc*sizeof(float),cudaMemcpyHostToDevice);

	float *d_omega;
	cudaMalloc((void**)&d_omega,nc*sizeof(float));
	cudaMemcpy(d_omega,omega,nc*sizeof(float),cudaMemcpyHostToDevice);

	float *d_delta;
	cudaMalloc((void**)&d_delta,nc*nc*sizeof(float));
	cudaMemcpy(d_delta,delta,nc*nc*sizeof(float),cudaMemcpyHostToDevice);

	//Launch the kernel that computes the new positions of the particles
	kernel_randomWalk<<<numBlocks, numThreadsPerBlock>>>(Nparticles,Niter,d_x_in,d_y_in,d_z_in,dt,up,vp,wp, devStates, nc, fp_type, d_m, T, P, Sa, Ta, d_Mol_wt, d_Pc, d_Tc, d_omega, d_delta, d_x_out,d_y_out,d_z_out);

	//kernel that computes the new position of the particles with uniform veloctiy, not used
	//kernel_uniformVelocity<<<numBlocks, numThreadsPerBlock>>>(Nparticles,d_z_in, dt, u_slip, d_z_out);


	//copy the result from the device to the host
	cudaMemcpy(x_out,d_x_out,Nparticles*sizeof(float),cudaMemcpyDeviceToHost);
	cudaMemcpy(y_out,d_y_out,Nparticles*sizeof(float),cudaMemcpyDeviceToHost);
	cudaMemcpy(z_out,d_z_out,Nparticles*sizeof(float),cudaMemcpyDeviceToHost);

	//free the GPU allocated memory
	cudaFree(d_x_in);
	cudaFree(d_x_out);
	cudaFree(d_y_in);
	cudaFree(d_y_out);
	cudaFree(d_z_in);
	cudaFree(d_z_out);
	cudaFree(devStates);
	cudaFree(d_m);
	cudaFree(d_Mol_wt);
	cudaFree(d_Pc);
	cudaFree(d_Tc);
	cudaFree(d_omega);
	cudaFree(d_delta);
}
