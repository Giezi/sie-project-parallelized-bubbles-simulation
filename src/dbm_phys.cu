#include <cmath>
#include <cstdio>

#define VERBOSE 0

#define  DP 8
#define  G 9.81
#define PI 3.141592653589793

__device__ float eotvos(float de, float rho_p, float rho, float sigma)
{
  return G * (rho - rho_p) * pow(de,2) / sigma;
}

__device__ float morton(float rho_p, float rho, float mu, float sigma)
{
  return G * pow(mu,4) * (rho - rho_p) / (pow(rho,2) * pow(sigma , 3));
}

__device__ float h_parameter(float Eo, float M, float mu)
{
  return 4.0 / 3.0 * Eo * pow(M, (float)-0.149) * pow((mu / (float)0.0009),(float)-0.14);
}

__device__ int particle_shape(float de, float rho_p, float rho, float mu, float sigma)
{
  float Eo = eotvos(de, rho_p, rho, sigma);
  float M = morton(rho_p, rho, mu, sigma);
  float H = h_parameter(Eo, M, mu);
  if(H < 2.0)
  {
    return 1;
  }else if(Eo < 40.0 && M < 0.001 && H < 1000.0)
  {
    return 2;
  }else
  {
    return 3;
  }
}

__device__ float us_sphere(float de, float rho_p, float rho, float mu)
{
  float Nd = 4.0 * rho * abs(rho - rho_p) * G * pow(de, 3) / (3.0 * pow(mu,2));
  float W = log10(Nd);
  float Re;
  if (Nd <= 73.0)
    Re = (Nd / 24.0 - 1.7569e-4 * pow(Nd,2) + 6.9252e-7 * pow(Nd,3) - 2.3027e-10 * pow(Nd,4));
  else if (Nd <= 580.0)
    Re = pow(10.0,(-1.7095 + 1.33438 * W - 0.11591 * pow(W,2)));
  else if (Nd <= 1.55e7)
    Re = pow(10.0,(-1.81391 + 1.34671 * W - 0.12427 * pow(W,2) + 0.006344 * pow(W,3)));
  else if (Nd <= 5.0e10)
    Re = pow(10.0,(5.33283 - 1.21728 * W + 0.19007 * pow(W,2) - 0.007005 * pow(W,3)));
  else
  {
    Re = NAN;
  }

  // return the slip velocity
  return mu / (rho * de) * Re;
}

__device__ float us_ellipsoid(float de, float rho_p, float rho, float mu, float sigma)
{
  float J, Eo, M, H, Re;
  // Calculate the non-dimensional variables
  Eo = eotvos(de, rho_p, rho, sigma);
  M = morton(rho_p, rho, mu, sigma);
  H = h_parameter(Eo, M, mu);

  // Compute the correlation equations
  if (H > 59.3)
      J = 3.42 * pow(H,(float)(0.441));
  else
      J = 0.94 * pow(H,(float)(0.757));

  // Calculate the Reynolds number
  Re = pow(M,(float)(-0.149)) * (J - 0.857);

  // Return the slip velocity
  return  mu / (rho * de) * Re;
}

__device__ float us_spherical_cap(float de, float rho_p, float rho)
{
  return 0.711 * sqrt(G * de * (rho - rho_p) / rho);
}
