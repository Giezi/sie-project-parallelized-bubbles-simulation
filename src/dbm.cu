# include <dbm_eos.cu>
# include <dbm_phys.cu>
# include <seawater.cu>
# include <cstdio>
# include <cmath>
# define VERBOSE 0

#define PI 3.141592653589793

__device__ void dbm_densityArray(int nc, float* m, float T, float P, float* rho_p, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta)
{
  density(nc, T, P, m, Mol_wt, Pc, Tc, omega, delta, rho_p);
}

__device__ float dbm_densityValue(int nc, int fp_type, float* m, float T, float P, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta)
{
  float rho_p[2];
  dbm_densityArray(nc, m, T, P, rho_p, Mol_wt, Pc, Tc, omega, delta);
  return rho_p[fp_type];
}

__device__ float dbm_diameter(int nc, int fp_type, float* m, float T, float P, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta)
{
  int i;
  float sum_m = 0.0;
  float density = dbm_densityValue(nc, fp_type, m, T, P, Mol_wt, Pc, Tc, omega, delta);
  for(i = 0;  i < nc;  i++)
  {
    sum_m += m[i];
  }
  return pow((6.0 * sum_m / (PI * density)),(1.0/3.0));
}

__device__ void dbm_particle_shape(int nc, int fp_type, float* m, float T, float P, float Sa, float Ta, int* shape, float* de, float* rho_p, float* rho, float* mu, float* sigma, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta)
{
  *de = dbm_diameter(nc, fp_type, m, T, P, Mol_wt, Pc, Tc, omega, delta);
  *rho_p = dbm_densityValue(nc, fp_type, m, T, P, Mol_wt, Pc, Tc, omega, delta);
  *rho = seaDensity(Ta, Sa, P);
  *mu = seaMu(Ta);
  *sigma = seaSigma(Ta);
  *shape = particle_shape(*de, *rho_p, *rho, *mu, *sigma);
}

__device__ float dbm_slip_velocity(int nc, int fp_type, float* m, float T, float P, float Sa, float Ta, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta)
{
  float us;
  float de, rho_p, rho, mu, sigma;
  int shape;
  dbm_particle_shape(nc, fp_type, m, T, P, Sa, Ta, &shape, &de, &rho_p, &rho, &mu, &sigma, Mol_wt, Pc, Tc, omega, delta);

  if (shape == 1)
      us = us_sphere(de, rho_p, rho, mu);
  else if (shape == 2)
      us = us_ellipsoid(de, rho_p, rho, mu, sigma);
  else
      us = us_spherical_cap(de, rho_p, rho);

  return us;
}
