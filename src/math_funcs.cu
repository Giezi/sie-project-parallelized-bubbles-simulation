// This code computes the cubic roots of a polynome
# include "math.h"

__device__ void cubic_roots(float * p, float* roots)
{
  int i;
  float a = p[0];
  float b = p[1];
  float c = p[2];
  float d = p[3];

  float Delta = 18.0 * a * b * c * d - 4.0 * pow(b,3) * d + pow(b,2) * pow(c,2) - 4.0 * a * pow(c,3) - 27.0 * pow(a,2) * pow(d,2);

  float Dk[3];

  Dk[0] = pow(b,2) - 3.0 * a * c;
  Dk[1] = 2.0 * pow(b,3) - 9.0 * a * b * c + 27.0 * pow(a,2) * d;
  Dk[2] = -27.0 * pow(a,2) * Delta;
  float u[6];
  u[0] = 1.0;
  u[3 + 0] = 0.0;
  u[1] = -1.0 / 2.0;
  u[3 + 1] = sqrt(3.0) / 2.0;
  u[2] = -1.0 / 2.0;
  u[3 + 2] = - sqrt(3.0) / 2.0;
  float C0[2];
  if(Dk[2] >= 0)
  {
    C0[0] = pow((Dk[1] + sqrt(Dk[2])) / (float)2.0 , (float)(1.0/3.0));
    C0[1] = 0.0;
  }else
  {
    float root[2];
    root[0] = Dk[1] / 2.0;
    root[1] = sqrt(-Dk[2]) / 2.0;
    float r = sqrt(pow(root[0], 2) + pow(root[1], 2));
    float theta = atan(root[1] / root[0]);
    C0[0] = pow(r, (float)(1.0 / 3.0)) * cos(1.0 / 3.0 * theta);
    C0[1] = pow(r, (float)(1.0 / 3.0)) * sin(1.0 / 3.0 * theta);
  }

  for(i = 0;i < 3;i++)
  {
    roots[i] = -1.0 / (3.0 * a) * (b + u[i] * C0[0] + Dk[0] / (u[i] * C0[0]));
    roots[3 + i] = -1.0 / (3.0 * a) * (b + u[3 + i] * C0[1] + Dk[0] / (u[3 + i] * C0[1]));
  }
}
