#include <curand_kernel.h>
#include <dbm.cu>
#include <stdio.h>

#define VERBOSE 0

//this is the kernel that initiates the random numbers (called from host (CPU))
__global__ void kernel_setupRandom ( curandState * state, unsigned long seed )
{
    int id = blockIdx.x * blockDim.x + threadIdx.x;
    curand_init ( seed, id, 0, &state[id] );
}


//this is the kernel that calls for random numbers from the device (GPU)
__device__ float generateRandomNormal( curandState* globalState ,int ind)
{
    curandState localState = globalState[ind];
    float RANDOM = curand_normal( &localState );
    globalState[ind] = localState;
    return RANDOM;
}


//this is the kernel that computes the random walk, called from the CPU
__global__ void kernel_randomWalk(int Nparticles,int Niter,float* x_in,float* y_in,float* z_in, float dt,float up,float vp,float wp,curandState*globalState, int nc, int fp_type, float* m, float T, float P, float Sa, float Ta, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta, float* x_out, float* y_out, float* z_out) {
	int id = threadIdx.x + blockDim.x*blockIdx.x;

	if(id < Nparticles)
	{
		x_out[id] = x_in[id];
		y_out[id] = y_in[id];
		z_out[id] = z_in[id];
		for(int k = 0;k<Niter;k++)
		{
			// generate random numbers for the 3 dimensions
			float p1 = generateRandomNormal(globalState,id);
			float p2 = generateRandomNormal(globalState,id);
			float p3 = generateRandomNormal(globalState,id);
      // compute u_slip veloctiy
      float u_slip = dbm_slip_velocity(nc, fp_type, m, T, P, Sa, Ta, Mol_wt, Pc, Tc, omega, delta);
      //compute the new positions for the 3 dimensions
      x_out[id] = x_out[id] + up * p1 * dt;
      y_out[id] = y_out[id] + vp * p2 * dt;
      z_out[id] = z_out[id] + wp*p3 * dt + u_slip*dt;

      // print if verbose, synchronize threads before printing
      if(VERBOSE) __syncthreads();
      if(VERBOSE) printf("%d: %f, %d of %d\n", id, x_out[id], k, Niter);
      if(VERBOSE) printf("%d: %f, %d of %d\n", id, y_out[id], k, Niter);
      if(VERBOSE) printf("%d: %f, %d of %d\n", id, z_out[id], k, Niter);
      if(VERBOSE) __syncthreads();
		}

	}
}



//this is the example kernel with uniform velocity and no random walk, currentily commented because not used
/*void __global__ kernel_uniformVelocity(int Nparticles,float* x_in, float dt, float u, float* x_out) {
	int id = threadIdx.x + blockDim.x*blockIdx.x;

	if(id < Nparticles)
	{
		printf ("tid %d. z_in: %f\n", id, x_in[id]);
		x_out[id] = x_in[id]+u*dt;
		printf ("tid %d. z_in: %f\n", id, x_out[id]);
	}
}*/
