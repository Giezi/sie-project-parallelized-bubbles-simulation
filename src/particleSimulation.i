/* -*- C -*-  (not really, but good for syntax highlighting) */
//this is the function that defines what swig has to wrap to python

//define the module
%module particleSimulation

//include the headers
%{
    #define SWIG_FILE_WITH_INIT
    #include "particleSimulation.h"
%}

//include the typemaps
%include "numpy.i"

//initiate the import of the arrays from python to c
%init %{
    import_array();
%}

//define which typemap corresponds to which variable (input)
%apply (int DIM1, float* IN_ARRAY1) {(int Nparticles,float* x_in),(int Nparticlesy,float* y_in),(int Nparticlesz,float* z_in),(int nc,float* m), (int nMol_wt, float* Mol_wt), (int nPc,float* Pc), (int nTc,float* Tc), (int nOmega, float* omega), (int nDelta, float* delta)}
%apply (int IN_ARRAY1) {(int Niter), (int fp_type)}
%apply (float IN_ARRAY1) {(float dt),(float up),(float vp),(float wp),(float T),(float P),(float Sa),(float Ta)}

//define the output arguments
%apply (int DIM1,float* ARGOUT_ARRAY1) {(int nx,float* x_out),(int ny,float* y_out),(int nz,float* z_out)};

//include the header
%include "particleSimulation.h"


// redirect call to doCUDAcomp to my_doCUDAcomp to do error assesment and remove unused variables needed by the python-cuda interface
%rename (doCUDAcomp) my_doCUDAcomp;

%inline %{

    void my_doCUDAcomp(int Nparticles,float* x_in,int Nparticlesy,float* y_in,int Nparticlesz,float* z_in,int nc, float* m, int nMol_wt, float * Mol_wt,int nPc, float* Pc,int nTc, float* Tc, int nOmega, float* omega, int nDelta, float* delta,int Niter, int fp_type,float dt,float up,float vp,float wp, float T, float P, float Sa, float Ta, int nx, float* x_out, int ny, float* y_out, int nz, float* z_out)
    {
        // Check that all the variable sizes are alright
        if(Nparticles != Nparticlesy || Nparticles != Nparticlesz)
            PyErr_Format(PyExc_ValueError, "Length of x_in(%d), y_in(%d) and z_in(%d) must be the same\n",Nparticles,Nparticlesy,Nparticlesz);

        if(Nparticles != nz || Nparticles != ny || Nparticles != nx)
            PyErr_Format(PyExc_ValueError, "Length of x_out(%d), y_out(%d) and z_out(%d) must be the same as the length of the input vector(%d)\n",nx,ny,nz,Nparticles);

        if(nc != nMol_wt || nc != nPc || nc != nTc || nc != nOmega || nc*nc != nDelta)
            PyErr_Format(PyExc_ValueError, "There must be the same number of components for each property [nc(%d), Mol_wt(%d), Pc(%d), Tc(%d)], omega(%d)], delta(%d)]\n",nc,nMol_wt,nPc,nTc,nOmega,nDelta/nc);

        doCUDAcomp(Nparticles,x_in,y_in, z_in,nc, m, Mol_wt, Pc, Tc, omega, delta,Niter, fp_type,dt,up,vp,wp, T, P, Sa, Ta, x_out, y_out, z_out);
    }

%}
