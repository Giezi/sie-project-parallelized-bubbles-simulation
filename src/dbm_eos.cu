#include <cmath>
#include <math_funcs.cu>
#include <cstdio>

#define VERBOSE 0
#define DP 8
#define RU 8.31451

__device__ void mole_fraction(int nc, float* mass, float* Mol_wt, float*yk)
{
  int k;
  float s_n_moles = 0.0;
  for(k = 0;k<nc;k++)
  {
    yk[k] = mass[k] / Mol_wt[k];
    s_n_moles += yk[k];
  }
  for(k = 0;k<nc; k++)
  {
    yk[k] /= s_n_moles;
    if(VERBOSE) printf("computed yk[%d] %f\n",k,yk[k]);
  }
}

__device__ void coefs(int nc, float T, float P, float* mass, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta, float* A, float* B, float* Ap, float* Bp, float* yk)
{
  //Convert the masses to mole fraction
  mole_fraction(nc, mass, Mol_wt, yk);
  //dynamically allocate memory
  float* mu = (float*) malloc(nc);
  memset(mu,0,nc);
  float* alpha = (float*) malloc(nc);
  memset(alpha,0,nc);
  float* aTk = (float*) malloc(nc);
  memset(aTk,0,nc);
  float* bk = (float*) malloc(nc);
  memset(bk,0,nc);
  float* s_Ap = (float*) malloc(nc);
  memset(s_Ap,0,nc);
  float bd = 0.0, aT = 0.0;
  int k,j;

  for(k = 0;k<nc;k++)
  {
    mu[k] = 0.37464 + 1.54226 * omega[k] - 0.26992 * pow(omega[k],2);
    alpha[k] = pow((1.0 + mu[k] * (1.0 - sqrt(T / Tc[k]))),2);
    aTk[k] = 0.45724 * pow(RU,2) * pow(Tc[k],2) / Pc[k] * alpha[k];
    bk[k] = 0.0778 * RU * Tc[k] / Pc[k];
    bd += yk[k];
  }

  //use the mixing rules in McCain (1990)
  for(k = 0;k<nc;k++)
  {
    for(j = 0;j<nc;j++)
    {
      aT += yk[k] * yk[j] * sqrt(aTk[k] * aTk[j]) * (1.0 - delta[k*nc+j]);
      s_Ap [k] += yk[j] * sqrt(aTk[j]) * (1.0 - delta[j*nc+k]);
    }
  }
  //Compute the coefficients of the polynomials for z-factor and fugacity
  *A = aT * P / (pow(RU,2) * pow(T,2));
  *B = bd * P / (RU * T);

  for(k = 0;k<nc;k++)
  {
    Bp[k] = bk[k] / bd;
    Ap[k] = 1.0 / aT * (2.0 * sqrt(aTk[k]) * s_Ap[k]);
  }
  free(mu);
  free(alpha);
  free(aTk);
  free(bk);
  free(s_Ap);
}

__device__ void z_pr(int nc, float T, float P, float* mass, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta, float *A, float *B, float* z ,float* Ap, float* Bp, float* yk)
{
  if(VERBOSE) printf("entered z_pr computation part\n");
  int i;
  float p_coefs[4];
  float z_roots[6];

  coefs(nc, T, P, mass, Mol_wt, Pc, Tc, omega, delta, A,B, Ap, Bp, yk);
  if(VERBOSE) printf("computed coefs\n");
  p_coefs[0] = 1.0;
  p_coefs[1] = *B - 1.0;
  p_coefs[2] = *A - 2.0 * *B - 3.0 * pow(*B,2);
  p_coefs[3] = pow(*B,3) + pow(*B,2) - *A * *B;

  cubic_roots(p_coefs,z_roots);

  // Extract the correct z-factors
  float z_max = 0.0;
  for(i = 0; i < 3; i++)
  {
    if ((z_roots[3 + i]) <= 1e-10)
    {
      if ((z_roots[i]) > z_max)
      {
          z_max = (z_roots[i]);
      }
    }
  }
  float z_min = z_max;
  for(i = 0; i < 3; i++)
  {
    if ((z_roots[3 + i]) <= 1e-10)
    {
      if ((z_roots[i]) > z_min)
      {
          z_min = (z_roots[i]);
      }
    }
  }

  // Return the z-factors in z
  z[0] = z_max;
  z[1] = z_min;
}

__device__ void density(int nc, float T, float P, float* mass, float * Mol_wt, float* Pc, float* Tc, float* omega, float* delta, float* rho)
{
  int i;
  float z[2];
  // dynamically allocate memory
  float A, B, R;
  float* Ap = (float*) malloc(nc);
  memset(Ap,0,nc);
  float* Bp = (float*) malloc(nc);
  memset(Bp,0,nc);
  float* yk = (float*) malloc(nc);
  memset(yk,0,nc);
  z_pr(nc, T, P, mass, Mol_wt, Pc, Tc, omega, delta, z, &A, &B, Ap, Bp, yk);
  // Compute and return the density
  R = 0.0;
  for(i = 0; i < nc; i++)
  {
    R += mass[i] / Mol_wt[i];
  }
  float mass_sum = 0.0;
  for(i = 0; i < nc; i++)
  {
    mass_sum += mass[i];
  }
  R = RU * R / mass_sum;

  for(i = 0;i < 2;i++)
  {
    rho[i] = P / (z[i] * R * T);
  }
  free(Ap);
  free(Bp);
  free(yk);
}
