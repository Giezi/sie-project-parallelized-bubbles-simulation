import numpy as np
#import custom class
import parallelParticles as pp
import time
#import matplotlib.pyplot as plt
#define the number of particles to simulate
nbParticles = 100;
#define initial position of the particles
x = np.zeros(nbParticles, dtype=np.float32);
y = np.zeros(nbParticles, dtype=np.float32);
z = np.zeros(nbParticles, dtype=np.float32);
#store the value in matrix
X = x;
Y = y;
Z = z;
#define the number of time-steps to simulate in Python
iterations = 10;
#define number of time steps to simulate in CUDA
timeSteps = 10
#define speeds
dt = 1.0
up = 1.0
vp = 1.0
wp = 1.0
#define constants
T = 278.15
P = 1000
Sa = 35.0
Ta = T
#composition: mehtan,ethan,propan
m = np.array([1,1,1], dtype=np.float32)
Mol_wt = np.array([0.016043,0.03007,0.044097], dtype=np.float32)
Pc = np.array([4599000,4872000,4248000], dtype=np.float32)
Tc = np.array([190.56,305.32,369.83], dtype=np.float32)
omega = np.array([0.011,0.099,0.152], dtype=np.float32)
#run the simulation
for n in range(0,iterations):
	#launch kernel function
	#t=time.time()
	(x,y,z) = pp.getNewParticlePosition(x,y,z,timeSteps,dt,up,vp,wp,T, P,Sa,Ta,m,Mol_wt,Pc,Tc,omega)
	#print(time.time()-t)
	#save array to matrix
	X=np.vstack([X,x])
	Y=np.vstack([Y,y])
	Z=np.vstack([Z,z])

#print(X)
#print(Y)
#print(Z)
#save result
np.savetxt('results/X.csv', X, delimiter=',')
np.savetxt('results/Y.csv', Y, delimiter=',')
np.savetxt('results/Z.csv', Z, delimiter=',')


#print(A)
#plt.plot(np.c_[0:timeSteps+1], A)
#plt.plot(np.r_[1:10],np.r_[0:9])
#plt.show()
